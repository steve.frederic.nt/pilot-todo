# Welcome to Pilot Todo
![Version](https://img.shields.io/badge/version-0.1.2-blue.svg?cacheSeconds=2592000)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/steve.frederic.nt/pilot-todo)
[![License: GPLV3](https://img.shields.io/badge/License-GPLV3-yellow.svg)](https://www.gnu.org/licenses/gpl-3.0.en.html)

> **Pilot Todo** is a Proof-of-Concept to show the interaction of Docker, Kubernetes, and Helm in order to deploy a ReactJS Todo App programatically. It is not meant for a production environment and most likely has some bugs.

### 🏠 [Homepage](https://gitlab.com/steve.frederic.nt/pilot-todo)

## Requirements:
* [Bash](https://www.gnu.org/software/bash/)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/)
* A Local Kubernetes Cluster like [Minikube](https://minikube.sigs.k8s.io/docs/start/) with [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
* [Helm (v3)](https://helm.sh/docs/intro/install/)
* [Task:](https://taskfile.dev/#/installation) A task runner / simpler Make alternative written in Go
* A DNS configuration to simulate a domaine name you manage locally: You can edit your /etc/hosts file or follow [Minikube's documentation](https://taskfile.dev/#/installation) on the matter

## Help

**Docker:**
Start:
Using your favorite terminal and docker directory, run:

```sh
docker-compose up -d
```

You will be able to have access to the app at: [http://localhost:3000](http://localhost:3000).

Stop:
```sh
docker-compose down
```
(In the docker directory)

**Kubernetes**
From the k8s directory:

```sh
task: Available tasks for this project:

* help:         Get a list of possible commands (tasks)
* k8s-start:    Start Minikube k8s cluster
* k8s-stop:     Stop and cleanup Minikube k8s cluster
* todo-install: Install the helm package onto the k8s cluster
* todo-remove:  Remove the helm package from the k8s cluster

```
Enjoy!

## Author

👤 **Steve N-T**

* Website: TODO
* Gitlab: [@steve.frederic.nt](https://gitlab.com/steve.frederic.nt)
* LinkedIn: [@https:\/\/www.linkedin.com\/in\/steve-nt](https://linkedin.com/in/https:\/\/www.linkedin.com\/in\/steve-nt)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/steve.frederic.nt/pilot-todo/-/issues).


## 📝 License

Copyright © 2022 [Steve N-T](https://gitlab.com/steve.frederic.nt).

This project is [GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html) licensed.